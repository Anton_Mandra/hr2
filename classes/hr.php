<?php

class Hr implements SplObserver {

    private $employee = array();

    public function hire(Human $human) {
        $this->employee[] = $human;
        $human->setProp('boss', $this);
        if ($human->getProp('mate')['value']!==null){
            $human->getProp('mate')['value']->attach('phone', $this);
        }
    }

    public function update($subject) {

        echo $subject->getProp('mate')['value']
                ->getProp('name')['value'] . ": "
                . $subject->getProp('phone')['value'] . "<br>\n";
    }

}
