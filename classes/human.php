<?php

class Human {

    private $name = array('value' => null,
        'observers' => []);
    private $phone = array('value' => null,
        'observers' => []);
    private $boss = array('value' => null,
        'observers' => []);
    private $mate = array('value' => null,
        'observers' => []);

    public function __construct($name, $phone) {

        $this->name['value'] = $name;
        $this->phone['value'] = $phone;
    }

    public function setProp($name, $value) {

        $this->$name['value'] = $value;

        if (!empty($this->$name['observers'])) {
            foreach ($this->$name['observers'] as $observer) {
                $this->notify($observer);
            }
        }
    }

    public function getProp($name) {

        return $this->$name;
    }

    private function attachOne($name, SplObserver $observer) {

        $this->$name['observers'][] = $observer;
        return true;
    }

    private function attachArray($name, array $observers) {
        foreach ($observers as $observer) {
            $this->attachOne($name, $observer);
        }
        return true;
    }

    public function attach($name, $observer) {


        if (is_array($observer)) {
            $this->attachArray($name, $observer);
        } else {
            $this->attachOne($name, $observer);
        }
    }

    private function detachOne($name, SplObserver $observer) {

        $key = array_search($observer, $this->$name['observers'], true);
        if ($key !== false) {
            unset($this->$name['observers'][$key]);
            return true;
        } else {
            echo "There is no such observer<br>\n";
            return false;
        }
    }

    private function detachArray($name, array $observers) {

        foreach ($observers as $observer) {
            $this->detachOne($name, $observer);
        }
        return true;
    }

    public function detach($name, $observer) {

        if (is_array($observer)) {
            $this->detachArray($name, $observer);
        } else {
            $this->detachOne($name, $observer);
        }
    }

    public function notify($observer) {
        $observer->update($this);
    }

    public function wedding($mate) {
        if ($this->mate['value'] !== null || $mate->mate['value'] !== null || $this === $mate) {
            echo "wedding is impossible";
            return false;
        } else {
            $this->setProp('mate', $mate);
            $mate->setProp('mate', $this);
            if ($this->boss['value']!==null){
                $mate->attach('phone',$this->boss['value'] );
            }
            echo $this->name['value'] .
            ' & ' . $mate->name['value'] .
            ' just married' . "<br>\n";
            return true;
        }
    }

}
